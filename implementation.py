import random
from example_structure import Dog, Human
from fuzzer_setup import setup_for_fuzzing


example = Human('Johnny', 20, Dog('Zoey', 11))
print example.to_string()
setup_for_fuzzing(example)

for n in range(100):
    new_human = Human.generate()
    print new_human.to_string()
