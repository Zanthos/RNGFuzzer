class Dog(object):

    # Class Attribute
    species = 'mammal'

    # Initializer / Instance Attributes
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def to_string(self):
        return ' species: ' + self.species + \
               ' name: ' + self.name + \
               ' age: ' + str(self.age)


class Human(object):

    species = 'mammal'

    def __init__(self, name, age, dog):
        self.name = name
        self.age = age
        self.dog = dog

    def to_string(self):
        return ' species: ' + self.species + \
               ' name: ' + self.name + \
               ' age: ' + str(self.age) + \
               self.dog.to_string()
