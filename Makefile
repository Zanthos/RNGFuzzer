output: rconfig.o
		g++ -std=c++11 -Wall rconfig.o -o output

 rconfig.o: rconfig.cpp config.h
		g++ -std=c++11 -c rconfig.cpp

 clean:
		rm *.o output