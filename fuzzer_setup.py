import inspect
import pexpect
from subprocess import Popen, PIPE

analyzer = pexpect.spawn('./output')

def setup_for_fuzzing(ex_class):
    attributes = inspect.getmembers(ex_class, lambda atr: not(inspect.isroutine(atr)))
    refined = [atr for atr in attributes if not(atr[0].startswith('__') and atr[0].endswith('__'))]

    properties = []
    classes = []
    for attribute in refined:
        if not isinstance(attribute[1], (int, long, float, str, complex)):
            setup_for_fuzzing(attribute[1])
            classes.append(attribute)
        else:
            # print attribute[0] + ' - ' + str(type(attribute[1]))
            properties.append(attribute)

    @classmethod
    def generate(cls):
        self = cls.__new__(cls)
        for prop in properties:
            analyzer.sendline( prop[0] )
            analyzer.readline() # skips input
            test = analyzer.readline()[:-2]
            setattr(self, prop[0], type(prop[1])(test))
        for obj in classes:
            setattr(self, obj[0], type(obj[1]).generate())
        return self
    ex_class.__class__.generate = generate
