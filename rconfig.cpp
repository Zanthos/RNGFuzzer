#include <random>
#include <string>
#include <iostream>

#include "config.h"

std::mt19937 rng;

char* getCmdOption(char ** begin, char ** end, const string & option)
{
    char ** itr = std::find(begin, end, option);
    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return 0;
}

Strategy findStrategy(Target target)
{
    auto it = config.find(target);
    if(it == config.end()) {
        std::uniform_int_distribution<int> dist(0,9);
        auto num = dist(rng);
        return Strategy{num};
    }
    else return config[target];
}

int main(int argc, char **argv)
{
    rng.seed(std::random_device()());

    // char * typePath = getCmdOption(argv, argv + argc, "-tp");
    // char * name = getCmdOption(argv, argv + argc, "-n");
    string line;
    while (true) {
        // std::cout << "\ninput: ";
        if(std::getline(std::cin, line)) {
            Strategy strat = findStrategy(Target{line});
            std::cout << strat.value << std::endl;
        }
    }
}