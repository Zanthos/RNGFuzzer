#include <map>

#define string std::string

struct target{
    string name;
};
typedef struct target Target;
bool operator <(const Target& x, const Target& y) {
    return x.name < y.name;
}

struct strategy{
    int value;
};
typedef struct strategy Strategy;

//struct entry{
//    Target target;
//    Strategy strategy;
//}
//typedef struct entry Entry;

using Config = std::map<Target, Strategy>;

Config config = { { {"age"}, { 90 } } };

//constexpr Entry test1 = { {"age"}, { 90 } }
//"age" : 90,

//auto test2 = { { unsigned int, string }, { 0.6, ["Johnny", "Rockett"] } }
//{ unsigned int, String } : { 0.6, ["Johnny", "Rockett"] }